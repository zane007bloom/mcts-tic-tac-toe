package za.co.entelect.bots.mcts;

import za.co.entelect.tictactoe.Board;
import za.co.entelect.tictactoe.Player;

import java.util.HashSet;
import java.util.Set;

public class Node {

    private Board board;

    private Node parent;
    private Set<Node> children = new HashSet<>();

    private int numberOfPlays;
    private int numberOfWins;
    private Player player;

    public Node(Node parent, Board board, Player player) {
        this.parent = parent;
        this.board = board;
        this.player = player;
    }

    public Node(Board board, Player player) {
        this.board = board;
        this.player = player;
    }

    public Node getParent() {
        return parent;
    }

    public Set<Node> getChildren() {
        return children;
    }

    public int getNumberOfPlays() {
        return numberOfPlays;
    }

    public Board getBoard() {
        return board;
    }

    public void addChild(Node child) {
        children.add(child);
    }

    public void increaseNumberOfPlays() {
        numberOfPlays++;
    }

    public void increaseNumberOfWins() {
        numberOfWins++;
    }

    public void decreaseNumberOfWins() {
        numberOfWins--;
    }

    public boolean hasParent() {
        return parent != null;
    }

    public float getWinAverage() {
        return numberOfWins / (float) numberOfPlays;
    }

    public boolean isTerminal() {
        return board.isGameOver();
    }

    public boolean isFullyExpanded() {
        return children.size() == board.getEmptyPositions().size();
    }

    public Player getPlayer() {
        return player;
    }

}
