package za.co.entelect.bots.mcts;

import za.co.entelect.tictactoe.Board;
import za.co.entelect.tictactoe.Player;

public interface Bot {

    Board calculateNextMove(Board board, Player player);

}
