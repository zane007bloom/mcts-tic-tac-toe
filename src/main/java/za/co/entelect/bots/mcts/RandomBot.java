package za.co.entelect.bots.mcts;

import za.co.entelect.tictactoe.Board;
import za.co.entelect.tictactoe.Player;

public class RandomBot implements Bot {

    @Override
    public Board calculateNextMove(Board board, Player player) {
        Board newBoard = new Board(board);
        newBoard.randomPlay(player);
        return newBoard;
    }

}
