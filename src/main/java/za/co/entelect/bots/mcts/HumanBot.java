package za.co.entelect.bots.mcts;

import za.co.entelect.tictactoe.Board;
import za.co.entelect.tictactoe.Player;
import za.co.entelect.tictactoe.Position;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class HumanBot implements Bot {

    private List<Position> positions = Arrays.asList(
            new Position(0, 0), new Position(1, 0), new Position(2, 0),
            new Position(0, 1), new Position(1, 1), new Position(2, 1),
            new Position(0, 2), new Position(1, 2), new Position(2, 2)
    );

    @Override
    public Board calculateNextMove(Board board, Player player) {
        Scanner scanner = new Scanner(System.in);
        System.out.println();
        System.out.println("Human Bot Selection");
        System.out.println();
        System.out.println("0 | 1 | 2");
        System.out.println("---------");
        System.out.println("3 | 4 | 5");
        System.out.println("---------");
        System.out.println("6 | 7 | 8");
        System.out.println();
        System.out.print("Enter a position: ");
        int index = scanner.nextInt();
        System.out.println();
        Position position = positions.get(index);
        Board newBoard = new Board(board);
        newBoard.performMove(player, position);
        return newBoard;
    }

}
