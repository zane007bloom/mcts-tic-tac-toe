package za.co.entelect.bots.mcts;

import za.co.entelect.tictactoe.Board;
import za.co.entelect.tictactoe.Player;
import za.co.entelect.tictactoe.Position;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Comparator.comparingDouble;
import static java.util.Comparator.comparingInt;

public class MonteCarloTreeSearchBot implements Bot {

    private static final long MAX_RUN_TIME = 100000000L;
    private static final double EXPLORATION = 5.5;

    @Override
    public Board calculateNextMove(Board board, Player player) {
        long start = System.nanoTime();
        Node root = new Node(board, getOpposite(player));
        while (System.nanoTime() - start < MAX_RUN_TIME) {
            Node interestingNode = selection(root, root);
            Node newNode = expand(interestingNode);
            Player winner = simulateRandomGame(newNode);
            backPropagate(newNode, winner);
        }
        return bestChildBasedOnNumberOfPlays(root).getBoard();
    }

    private Node selection(Node node, Node root) {
        // We need to pick an "interesting" node to expand, find a good balance between exploration and exploitation
        // We return the first terminal or non-fully expanded node we find,
        // Other wise we find the best child based on the ucb score, and run that node through selection
        if (node.isTerminal()) {
            return node;
        }
        if (!node.isFullyExpanded()) {
            return node;
        }
        return selection(bestChildBasedOnUCT(node, root.getNumberOfPlays()), root);
    }

    private Node expand(Node node) {
        // We expand out search space to further enhance our decision making effectiveness
        // If it's a terminal node we can't expand it
        // Otherwise we create a new child node, that has a new game board, with a random play from the opposite player
        // We just need to be careful not to add a duplicate, a node with the same game board as one of the other children
        if (node.isTerminal()) {
            return node;
        }
        Player oppositePlayer = getOpposite(node.getPlayer());
        Board newBoard = new Board(node.getBoard());
        List<Position> positionsToIgnore = node.getChildren().stream()
                .flatMap(c -> c.getBoard().getPlayerPositions(oppositePlayer).stream())
                .collect(Collectors.toList());
        newBoard.randomPlay(oppositePlayer, positionsToIgnore);
        Node newNode = new Node(node, newBoard, oppositePlayer);
        node.addChild(newNode);
        return newNode;
    }

    private Player simulateRandomGame(Node node) {
        // Now we simulate a random game, both players making random moves one after another.
        // If our node is a terminal node we just return the winner, no simulation is required
        // Otherwise we loop until the game is over, each player making random moves until there is a winner
        // We return the winner in order to do back-propagation
        if (node.isTerminal()) {
            return node.getBoard().getWinner();
        }

        Player player = getOpposite(node.getPlayer());

        Board tempBoard = new Board(node.getBoard());

        while (!tempBoard.isGameOver()) {
            tempBoard.randomPlay(player);
            player = getOpposite(player);
        }
        return tempBoard.getWinner();
    }

    private void backPropagate(Node node, Player winner) {
        // We have to update the rest of the tree to update the most recent result
        // We walk back up the tree, increasing the number of plays for each parent node
        // and updating the number of wins for the winner parent nodes
        // I noticed that decreasing the number of wins for loser parent nodes,
        // allows the better make better defensive choices in the later game
        node.increaseNumberOfPlays();
        if (node.getPlayer() == winner) {
            node.increaseNumberOfWins();
        } else if (getOpposite(node.getPlayer()) == winner) {
            node.decreaseNumberOfWins();
        }
        if (node.hasParent()) {
            backPropagate(node.getParent(), winner);
        }
    }

    private Node bestChildBasedOnNumberOfPlays(Node node) {
        return node.getChildren().stream().max(comparingInt(Node::getNumberOfPlays)).get();
    }

    private Node bestChildBasedOnUCT(Node node, int totalNumberOfPlays) {
        return node.getChildren().stream().max(comparingDouble(o -> uct(o, totalNumberOfPlays))).get();
    }

    private double uct(Node node, int totalNumberOfPlays) {
        // We calculate the UCB value
        return node.getWinAverage() + EXPLORATION * Math.sqrt(Math.log(totalNumberOfPlays) / node.getNumberOfPlays());
    }

    private Player getOpposite(Player player) {
        if (player == Player.O) {
            return Player.X;
        } else {
            return Player.O;
        }
    }

}
