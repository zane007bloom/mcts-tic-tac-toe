package za.co.entelect.tictactoe;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class Board {

    private static final int DEFAULT_BOARD_SIZE = 3;

    private Player[][] boardValues;

    public Board() {
        boardValues = new Player[DEFAULT_BOARD_SIZE][DEFAULT_BOARD_SIZE];
    }

    public Board(Board board) {
        int boardLength = board.getBoardValues().length;
        this.boardValues = new Player[boardLength][boardLength];
        Player[][] boardValues = board.getBoardValues();
        for (int y = 0; y < boardLength; y++) {
            for (int x = 0; x < boardLength; x++) {
                this.boardValues[y][x] = boardValues[y][x];
            }
        }
    }

    public void performMove(Player player, Position position) {
        boardValues[position.getY()][position.getX()] = player;
    }

    public Player[][] getBoardValues() {
        return boardValues;
    }

    public void printBoard() {
        System.out.println();
        int size = this.boardValues.length;
        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                System.out.print(boardValues[y][x] == null ? " " : boardValues[y][x]);
                if (x < size - 1) {
                    System.out.print(" | ");
                }
            }
            System.out.println();
            if (y < size - 1) {
                System.out.println("---------");
            }
        }
        System.out.println();
    }

    public List<Position> getEmptyPositions() {
        List<Position> emptyPositions = new ArrayList<>();
        for (int x = 0; x < boardValues.length; x++) {
            for (int y = 0; y < boardValues.length; y++) {
                if (boardValues[y][x] == null) {
                    emptyPositions.add(new Position(x, y));
                }
            }
        }
        return emptyPositions;
    }

    public Position getRandomEmptyPosition() {
        List<Position> emptyPositions = getEmptyPositions();
        return chooseRandom(emptyPositions);
    }

    public void randomPlay(Player player) {
        performMove(player, getRandomEmptyPosition());
    }

    public void randomPlay(Player player, List<Position> positionsToExclude) {
        List<Position> validPositions = getEmptyPositions();
        validPositions.removeAll(positionsToExclude);
        performMove(player, chooseRandom(validPositions));
    }

    private <T> T chooseRandom(List<T> elements) {
        int randomIndex = new Random().nextInt(elements.size());
        return elements.get(randomIndex);
    }

    public Collection<Position> getPlayerPositions(Player player) {
        List<Position> positions = new ArrayList<>();
        for (int y = 0; y < boardValues.length; y++) {
            for (int x = 0; x < boardValues.length; x++) {
                if (boardValues[y][x] == player) {
                    positions.add(new Position(x, y));
                }
            }
        }
        return positions;
    }

    public boolean isGameOver() {
        return getEmptyPositions().isEmpty() || getWinner() != null;
    }

    public Player getWinner() {
        Player winner = checkRows();
        if (winner != null) {
            return winner;
        }
        winner = checkCols();
        if (winner != null) {
            return winner;
        }
        winner = checkDiags();
        if (winner != null) {
            return winner;
        }
        return null;
    }

    private Player checkDiags() {
        boolean allSame = true;
        Player player = boardValues[0][0];
        int length = boardValues.length;
        for (int i = 1; i < length; i++) {
            if (boardValues[i][i] != player) {
                allSame = false;
            }
        }
        if (allSame) {
            return player;
        }
        player = boardValues[length - 1][0];
        allSame = true;
        for (int i = 1; i < length; i++) {
            if (boardValues[length - 1 - i][i] != player) {
                allSame = false;
            }
        }
        if (allSame) {
            return player;
        }
        return null;
    }

    private Player checkRows() {
        for (int y = 0; y < boardValues.length; y++) {
            boolean allSame = true;
            Player player = boardValues[y][0];
            for (int x = 1; x < boardValues.length; x++) {
                if (boardValues[y][x] != player) {
                    allSame = false;
                }
            }
            if (allSame && player != null) {
                return player;
            }
        }
        return null;
    }

    private Player checkCols() {
        for (int x = 0; x < boardValues.length; x++) {
            boolean allSame = true;
            Player player = boardValues[0][x];
            for (int y = 1; y < boardValues.length; y++) {
                if (boardValues[y][x] != player) {
                    allSame = false;
                }
            }
            if (allSame) {
                return player;
            }
        }
        return null;
    }

}