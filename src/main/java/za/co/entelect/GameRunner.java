package za.co.entelect;

import za.co.entelect.bots.mcts.Bot;
import za.co.entelect.bots.mcts.HumanBot;
import za.co.entelect.bots.mcts.MonteCarloTreeSearchBot;
import za.co.entelect.tictactoe.Board;
import za.co.entelect.tictactoe.Player;

public class GameRunner {

    public static void main(String[] args) {
        Bot botO = new MonteCarloTreeSearchBot();
        Bot botX = new HumanBot();
        Board board = new Board();

        Player player = Player.X;

        for (int i = 0; i < 9; i++) {
            if (player == Player.O) {
                board = botO.calculateNextMove(board, player);
            } else {
                board = botX.calculateNextMove(board, player);
            }
            System.out.println("Player " + player + "'s move");
            board.printBoard();
            if (board.isGameOver()) {
                break;
            }
            player = getOpposite(player);
        }

        System.out.println();
        System.out.println("Winner: " + (board.getWinner() == null ? "Draw" : board.getWinner()));
        System.out.println();

    }

    private static Player getOpposite(Player player) {
        if (player == Player.O) {
            return Player.X;
        } else {
            return Player.O;
        }
    }

}
