package za.co.entelect.bots.mcts;

import org.junit.Test;
import za.co.entelect.tictactoe.Board;
import za.co.entelect.tictactoe.Player;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class BotTest {

    @Test
    public void givenEmptyBoard_whenSimulateInterAIPlay_thenGameDraw() {
        Board board = new Board();
        List<Board> previous = new ArrayList<>();
        MonteCarloTreeSearchBot mcts = new MonteCarloTreeSearchBot();

        Player player = Player.O;
        int totalMoves = 9;
        for (int i = 0; i < totalMoves; i++) {
            board = mcts.calculateNextMove(board, player);
            previous.add(board);
            if (board.isGameOver()) {
                break;
            }
            player = getOpposite(player);
        }
        Player winner = board.getWinner();

        assertNull(winner);
    }

    @Test
    public void test() {
        Board board = new Board();
        board.getBoardValues()[1][0] = Player.X;
        board.getBoardValues()[1][1] = Player.X;
        board.getBoardValues()[1][2] = Player.X;

        board.getBoardValues()[2][0] = Player.O;
        board.getBoardValues()[2][1] = Player.O;
        board.getBoardValues()[2][2] = Player.O;

        assertNotNull(board.getWinner());
    }

    private Player getOpposite(Player player) {
        if (player == Player.O) {
            return Player.X;
        } else {
            return Player.O;
        }
    }

}